Gem::Specification.new do |s|
  s.name        = 'mycelia'
  s.version     = '0.0.0'
  s.date        = '2017-04-22'
  s.summary     = "Mycelia"
  s.description = ""
  s.authors     = ["Millicent Billette"]
  s.email       = 'millicent.b@agatas.org'
  s.files       = []
  s.homepage    = 'https://mycelia.tools'
  s.license     = 'AGPL-3.0'
end
